CA4 - Part 1

Docker Containers

1- First step os to downaload the docker desktop software.

2- After the installation process a WLS2 version was needed so i followed the update steps.

3- Once the update was finished the docker desktop was ready to be used, and a docker account was created with the 1211789 id.

4- First i used docker pull ubuntu to get an image file from the remote repository

5- After, a dockerfile was created with the basic commands needed to create a container with our specified necessities:
We need ubuntu to install all packages, git and java. Also, we download the needed repository. 
We need to change the permisiion to use graddlew, and afterwards we build the jar file.
A port is exposed (59001) so that the container can be acessed.
Finaly we run the command to execute the jar file and run the chaServer application.

The following dockerfile aws used: 

FROM ubuntu

RUN apt-get update -y
RUN apt-get install git -y
RUN apt-get install openjdk-11-jdk-headless -y
RUN git clone https://Cunha1211789@bitbucket.org/luisnogueira/gradle_basic_demo.git

WORKDIR gradle_basic_demo
RUN chmod u+x gradlew
RUN ./gradlew clean build

EXPOSE 59001

CMD java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

6- Using this dockerfile we use "docker build -t 'tag' . " to build the image to be used when starting a container.

7- When the image is built, we run the container using " docker run -p 59001:59001 -d 'tag' ". The -p 59001:59001 maps the ports in the localhost and the cointainer.

8- With this the container is running a chatServer app.

9- In my local machine i run the aplication using "gradlew runClient" command and i'm abble to connect to the chat.

Alternative way

1 - In an alternative way, instead of downloading all the aplications and their dependencies, the dockerfile was altered so that the .jar file is directy copied to the container while building the image.


dockerfile:

FROM ubuntu:18.04

RUN apt-get update -y

RUN apt-get install openjdk-11-jdk-headless -y

COPY basic_demo-0.1.0.jar .

EXPOSE 59001

CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001


2 - With the copy command we pass the .jar file directly to the created container, this way the container size will be a lot smaller and the setup process faster.



CA4 - Part 2  -  Compose .yaml file


1- For the second part of the exercise, docker compose was used. 

2- With compose, we use a YAML file to configure your application’s services. Then, with a single command, you create and start several services from the configuration file.

3- Docker compose defines several aspects of the containers, from their networks ip´s, fowarding ports and the container names.

4- For this exercise, two containers were created using docker compose. These were a webaplication and a database for data persistence.

5- The following docker compose file was used 

version: '3'
services:
  web:
    build: web
    ports:
      - "8080:8080"
    networks:
      default:
        ipv4_address: 192.168.56.10
    depends_on:
      - "db"
  db:
    build: db
    ports:
      - "8082:8082"
      - "9092:9092"
    volumes:
      - ./data:/usr/src/data-backup
    networks:
      default:
        ipv4_address: 192.168.56.11
networks:
  default:
    ipam:
      driver: default
      config:
        - subnet: 192.168.56.0/24


The compose file set up a local network for the two containers in the 192.168.56.0/24 IP.


6 - By running the 'docker compose up' command starts teh process and compiles the available dockerfiles. 

7- For the web aplication setup a few changes were necessary. Instead of only installing the tomcat server, i downloaded the full image for ubuntu with 'From ubuntu' command. This was needed
because the version of java used (11) had some imcompatibility with tomcat. 

8- Also i set the dockerfile to clone my repository and from it use the previous CA3 exercise version of the web aplication. The dockerfile builds the application from the available .war file.

9 - With the two containers running and acesseble via the local host trought the repective ports set up in the compose file, they were reday to be accessed.

10- Lastly the repository was push to the docker cloud using the 'docker tag' command, (docker tag 172cdfc1daab 1211789/ca_part1:tagname) and after
using 'docker push' (docker push 1211789/ca_part1:tagname).

11- 'docker pull 1211789/ca_part1' command can be used to download the repository.





Alternate software : kubernetes


Kubernetes is a complementary software to docker containers. While docker creates one or more isolated instances of selfsuficient containers, kubernetes can orquestrate when and where this containers 
will be execute trough an API. Kubernetes allows the user to organize a cluster of VM's and schedule the execution of containers in those VM depending on the available resources and the requirements 
of each container. Cointainers are grouped in pods, the base unit of operation of kubernetes. These containers can be scaled to the desired state, and manage their life cycle in order to mantain
the aplications running.


In order to use kubernetes the first step would be to download the software. 

The following toturial was obtained on how to configure a kubernetes cluster on a VM. It setps up a master node and a worker node(the mimnimum required).
The master node is responsible for maintaining the desired state of the cluster, such as which applications are running and which container images they use. 
Worker nodes actually run the applications and workloads. 




On master:

sudo hostnamectl set-hostname kubemaster
On worker node:

sudo hostnamectl set-hostname kubeworker
You now can easily distinguish which one is which from the names now, and you can go ahead and start installing packages for Kubernetes as follows.

On both machines:

Installation of Kubernetes helper packages:

sudo apt-get update && sudo apt-get install -y apt-transport-https gnupg2
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo “deb https://apt.kubernetes.io/ kubernetes-xenial main” | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
Update app repository:

sudo apt-get update
Install Kubernetes and packages and Docker on each machine:

sudo apt-get install -y kubectl kubeadm kubelet kubernetes-cni docker.io
Start Docker service:

sudo systemctl start docker
Enable Docker service so that the service will automatically resume whenever rebooted:

sudo systemctl enable docker
Start kubelet service:

sudo systemctl start kubelet
Enable kubelet service so that the service will automatically resume whenever rebooted:

sudo systemctl enable kubelet
Add the current user to the Docker group so that the Docker commands can be run with root privileges:

sudo usermod -aG docker $USER
newgrp docker
To enable the iptables of Linux Nodes to see bridged traffic correctly, please set net.bridge.dridge-nf-call-iptables to 1 in sysctl config and activate iptables as follows:

cat << EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sudo sysctl — system
On kubemaster:

Pull Kubernetes packages:

sudo kubeadm config images pull
Let kubeadm to prepare the environment:

sudo kubeadm init — apiserver-advertise-address=<private ip addres of kubemaster> — pod-network-cidr=172.16.0.0/16
(Please ensure to check your Master instance’s IP address and replace the same with <private ip addres of kubemaster> above)

You should now see the result along with the following lines:

Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
Alternatively, if you are the root user, you can run:

export KUBECONFIG=/etc/kubernetes/admin.conf
You should now deploy a pod network to the cluster.

Run kubectl apply -f [podnetwork].yaml with one of the options listed at:

https://kubernetes.io/docs/concepts/cluster-administration/addons/

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join 172.33.5.107:6443 — token 1aiej0.kf0t4on7c7bm2hlu \
— discovery-token-ca-cert-hash sha256:0e2abfb56733665c0e620423337f34be2a4f3c4b8d1ea44dff85666ddf722c02
Activate Calico pod networking:

kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
Setting up of control pane is now complete. You can now add slave / worker nodes to cluster.

On kubeworker:

Run following command to have each node to join the cluster:

kubeadm join 172.33.5.107:6443 — token 1aiej0.kf0t4on7c7bm2hlu \
— discovery-token-ca-cert-hash sha256:0e2abfb56733665c0e620423337f34be2a4f3c4b8d1ea44dff85666ddf722c02
On kubemaster:

Now you should be able to see the new workers in the list with the following command:

kubectl get nodes
Now you should get similar to the following on the screen:

NAME STATUS ROLES AGE VERSION
kubemaster Ready control-plane,master 6h47m v1.20.2
kubeworker Ready <none> 6h38m v1.20.2
You can obtain more info about the cluster using the following command:

kubectl get nodes -o wide
Take a deep breath; setting up the Kubernetes environment is now completed. You can run the cluster from the control plane’s command line. 
Depending on the tasks you need to accomplish with the Kubernetes cluster, you may need to import container images, effect deployments, create replica sets, 
increase/decrease the number of replicas in each set, set up horizontal auto-scaling, set up namespaces, and set up services.


