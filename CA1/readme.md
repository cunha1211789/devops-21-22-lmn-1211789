This is a readme file created in the CA1 folder using the windows cmd for class porpuses.


CA1 part 1 exercise steps:


In the previous class the git clone command was used in order to get the remote repository at the bitbucket server  


1-Created CA1 folder in the local repository

2-Copied the 'tut-react-and-spring-data-rest' into the CA1 folder.

3-Created a readme.md(this file) file in the CA1 folder

4-Used git add . command in order to add the changes made in the repository to this git repository standing area

5-Used git commit -m "commit message" to certify the changes and send the new version to the .git folder

6-Used git push to send update the remote repository in the bitbucket server 

7-Added a commit with the v1.1.0 tag to the commit

8-Made changes to the project backend, namely, added a job years field to the employee constructor

9-Created some tests to test the employee class

10-Tested the frontend browser page on the localhost:8080 and it loaded correctly

11-Made a git push with the changes to the remote repository with the v1.2.0 tag


Alternative VCS (mercurial)

1-Choose mercurial VCS to work with

2-Created an account on the sourceforge in order to create a remote repository

3-used the former excercise commands to add, commit and push a clone of the devOps folder to the remote. The commands where almost the same small changes, namely the use of "hg" instead of "git".

4-the repository can be checked using the following link - https://sourceforge.net/projects/devops-21-22-lmn-1211789/ - 



CA1 part 2 exercise steps

1- created a new branch called email-field using "git branch email-field"

2-switched to the new branch using "git checkout email-field"

3-made the required changes by adding an email field do the employee class

4-added a few tests 

5-used git add and git commit to certify the changes

6-checked out to the master branch and merged with the email-field branch. This presented some conflicts that required resolving.

7-Pushed the changes and added a v1.3.0 tag.

8-Created a new email-fix branch and the respective tests

9-Used git add. and git commit to certify the changes

10-Switched back to the master branch and merged the master branch with the email-fix branch

11-Used git push to send the changes to the remote repository and added the v1.3.1 tag


Alternative method (using mercurial)

1- Created a new branch using hg branch newBranch

2- Made some changes and used hg update 

3- used hg merge to merge the changes made to the default branch

4- pushed the changes to the remote


Final Note: Git and Mercurial end up being very similar, with Git being a bit more complex while mercurial has a simpler syntax. In terms of branching Mercurial is a bit more confusing. 
Also there is no index or staging area before the commit. Changes are committed as they are in the working directory.

