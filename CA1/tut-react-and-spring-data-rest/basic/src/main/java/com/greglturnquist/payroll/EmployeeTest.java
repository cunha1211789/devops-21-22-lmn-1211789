package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    public void shouldCreateEmployee() {
        Employee employee = new Employee("Frodo", "Baggins", "Hobbit", "Ring Bearer", 5 , "frodo@onering.com" );
        String firstName = "Frodo";
        String lastName = "Baggins";
        String description = "Hobbit";
        String jobTitle = "Ring Bearer";
        String email = "frodo@onering.com";
        int jobYears = 5;
        assertEquals(firstName, employee.getFirstName());
        assertEquals(lastName, employee.getLastName());
        assertEquals(description, employee.getDescription());
        assertEquals(jobTitle, employee.getJobTitle());
        assertEquals(jobYears, employee.getJobYears());
        assertEquals(email, employee.getEmail());
    }

    @Test
    public void shouldThrowExceptionEmptyFirstName() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("","Baggins", "Hobbit", "Ring Bearer", 5 ,"frodo@onering.com" );
        });
    }

    @Test
    public void shouldThrowExceptionEmptyLastName() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Frodo","", "Hobbit", "Ring Bearer", 5 ,"frodo@onering.com" );
        });
    }

    @Test
    public void shouldThrowExceptionEmptyDescription() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Frodo", "Baggins", "", "Ring Bearer", 5 ,"frodo@onering.com" );
        });
    }

    @Test
    public void shouldThrowExceptionEmptyJobTittle() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Frodo", "Baggins", "Hobbit", "", 5 ,"frodo@onering.com" );
        });
    }

    @Test
    public void shouldThrowExceptionNegativeNumber() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Frodo", "Baggins", "Hobbit", "", -5 ,"frodo@onering.com" );
        });
    }

    @Test
    public void shouldThrowExceptionNegativeEmail() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Frodo", "Baggins", "Hobbit", "", 5 ,"" );
        });
    }

    @Test
    public void shouldThrowExceptionEmail() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Frodo", "Baggins", "Hobbit", "", 5 ,"frodo" );
        });
    }
}