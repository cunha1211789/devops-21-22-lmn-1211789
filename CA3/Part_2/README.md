
# Relatório CA3 Parte 2 - Vagrant

**Nome:** Carolina Silva

**Número:** 1211753

---
#### Parte 2 - Vagrant

O Vagrant é uma ferramenta open source que permite gerir e manter criação automática de máquinas virtuais, com recurso a um hypervisor (por exmplo VirtualBox, ou VMWare). Utiliza o ficheiro Vagrantfile para efectuar a configuração da máquina virtual que se pretende criar. As máquinas virtuais são aprovisionadas por este hypervisor com recurso a ferramentas como a shell script, Chef ou Puppet que permitem instalar e configurar o software necessário.
O Vagrantfile é criado quando é colocado o comando **vagrant init** que contem bastante documentação e é um bom começo para se costumizar a(s) máquina(s) virtual(is).
Este ficheiro usa a lingagem Ruby.

#### 1. Usar vagrant-multi-spring-tut-demo

## Vagrant

O comando **vagrant** permite ver quais os comandos que podemos usar com o vagrant e o que eles executam:

    autocomplete    manages autocomplete installation on host
    box             manages boxes: installation, removal, etc.
    cloud           manages everything related to Vagrant Cloud
    destroy         stops and deletes all traces of the vagrant machine
    global-status   outputs status Vagrant environments for this user
    halt            stops the vagrant machine
    help            shows the help for a subcommand
    init            initializes a new Vagrant environment by creating a Vagrantfile
    login
    package         packages a running vagrant environment into a box
    plugin          manages plugins: install, uninstall, update, etc.
    port            displays information about guest port mappings
    powershell      connects to machine via powershell remoting
    provision       provisions the vagrant machine
    push            deploys code in this environment to a configured destination
    rdp             connects to machine via RDP
    reload          restarts vagrant machine, loads new Vagrantfile configuration
    resume          resume a suspended vagrant machine
    snapshot        manages snapshots: saving, restoring, etc.
    ssh             connects to machine via SSH
    ssh-config      outputs OpenSSH valid configuration to connect to the machine
    status          outputs status of the vagrant machine
    suspend         suspends the machine
    up              starts and provisions the vagrant environment
    upload          upload to machine via communicator
    validate        validates the Vagrantfile
    version         prints current and latest Vagrant version
    winrm           executes commands on a machine via WinRM
    winrm-config    outputs WinRM configuration to connect to the machine


O comando **vagrant init** permite criar o vagrantfile que permite configurar as máquians virtuais que pretendemos criar. Com este comando **vagrant init <nome da box>** permite criar máquinas virtuais com base em imagens/box já existentes.

    A `Vagrantfile` has been placed in this directory. You are now
    ready to `vagrant up` your first virtual environment! Please read
    the comments in the Vagrantfile as well as documentation on
    `vagrantup.com` for more information on using Vagrant.

O comando **vagrant up** permite arrancar a virtualização, ou seja, vai procurar vagrantfile e criar o ambiente virtual correspondente ao que está descrito neste ficheiro
No caso vai criar máquina virtual com ubuntu instalado, por default usa hypervisor virtualBox. Para criar as máquinas virtuais são usadas as chamadas "box" que são imagens de máquinas virtuais com um sistema operativo e algumas outras características (compatibilidade com hypervisor, ou versões da código java). Existe uma lista destas boxes em https://app.vagrantup.com/boxes/search . Estas box são criadas por utilizadores e estão disponíveis para serem usadas por qualquer um.

O comando **vagrant halt** permite parar a máquina virtual.

É possível a partilha de ficheiros entre a máquina real e a máquina virtual cria uma pasta /vagrant para partilha de ficheiros

O comando **vagrant ssh** permite entrar na VM criada através da máquina host.

#### 2. Estudar o Vagrantfile e perceber como são criadas as duas máquinas virtuais

##### .box e .configure
O Vagrantfile é criado quando é colocado o comando **vagrant init** que contem bastante documentação e é um bom começo para se costumizar a(s) máquina(s) virtual(is).
Este file usa a lingagem Ruby.

    Vagrant.configure("2") do |config|
     config.vm.box = "envimation/ubuntu-xenial"
    end

Aqui temos a configuração comando **.configure** que definimos como nome "config".
O seguinte comando:
        
    config.vm.box = "envimation/ubuntu-xenial"

É onde escolhemos a box (**.vm.box**) ou imagem que pretendemos usar na nossa máquina virtual. Neste caso vamos usar a *envimation/ubuntu-xenial* mas existe uma base de dados com imagens de máquinas virtuais já criadas por diferentes utilizadores (pode ser consultada em https://atlas.hashicorp.com/boxes/search). Estas box é uma imagem base da máquina virtual que queremos criar. Na máquina que criamos com esta base usamos uma cópia, ou seja, se a nossa máquina virtual for alterada ou destruida (comando **vagrant destroy** não fará qualquer alteração nas box original). Como todos os utilizadores podem criar uma box há algumas com qualidade baixa, a documentação do Vagrant sobre box oficiais fala que as que são do utilizador bento serão as melhores ou mais fiáveis.

O ficheiro Vagrantfile, como configura as máquinas virtuais criadas apenas corre na primeira vez que se faz **vagrant up**. Quando se faz uma alteração no Vagrantfile deve-se **vagrant reload** ou **vagrant destroy -f** (-f não pede confirmação antes de destruir) seguido de **vagrant up**. Se for mudado o aprovisionamento (provision) que vamos ver mais à frente deve ser usado o comando **vagrant up --provision**.

##### .provision

O aprovisionamento são os programas ou pacotes que pretendemos instalar na nossa ou nas várias máquinas virtuais criadas. Este é comum às máquinas criadas. Utilizamos comandos na shell para a instalação de pacotes na maquina virtual.

        config.vm.provision "shell", inline: <<-SHELL
        sudo apt-get update -y
        sudo apt-get install iputils-ping -y
        sudo apt-get install -y avahi-daemon libnss-mdns
        sudo apt-get install -y unzip
        sudo apt-get install openjdk-8-jdk-headless -y
        # ifconfig
        SHELL

Neste campo alterou-se a versão do java a ser instalada para *sudo apt-get install openjdk-11-jdk-headless -y*

##### configuração da primeira máquina virtual (db)

    config.vm.define "db" do |db|
    db.vm.box = "envimation/ubuntu-xenial"
    db.vm.hostname = "db"
    db.vm.network "private_network", ip: "192.168.56.11"
    
        # We want to access H2 console from the host using port 8082
        # We want to connet to the H2 server using port 9092
        db.vm.network "forwarded_port", guest: 8082, host: 8082
        db.vm.network "forwarded_port", guest: 9092, host: 9092
    
        # We need to download H2
        db.vm.provision "shell", inline: <<-SHELL
          wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar
        SHELL
    
        # The following provision shell will run ALWAYS so that we can execute the H2 server process
        # This could be done in a different way, for instance, setiing H2 as as service, like in the following link:
        # How to setup java as a service in ubuntu: http://www.jcgonzalez.com/ubuntu-16-java-service-wrapper-example
        #
        # To connect to H2 use: jdbc:h2:tcp://192.168.33.11:9092/./jpadb
        db.vm.provision "shell", :run => 'always', inline: <<-SHELL
          java -cp ./h2*.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists > ~/out.txt &
        SHELL
    end

O comando **config.vm.define "db" do |db|** inicia a configuração da máquina virtual
**db.vm.box = "envimation/ubuntu-xenial"**: define qual a box/imagem a usar nesta máquina virtual. Neste caso foi necessário alterar para:

    config.vm.box = "hashicorp/bionic64"
    config.vm.box_version = "1.0.282"

de modo a ser compatível com java jdk11.

**db.vm.hostname = "db"**: define o nome da máquina virtual criada neste caso vai chamar-se db
**db.vm.network "private_network", ip: "192.168.56.11"**: define a o ip da máquina na rede privada. Permite, comunicação com computadores que tenham endereços de ip nesta rede.

**db.vm.network "forwarded_port", guest: 8082, host: 8082**: definição do porto onde toda a informação/dados vão ser transferida entre a host e guest machine.
**db.vm.network "forwarded_port", guest: 9092, host: 9092**: definição do porto onde toda a informação/dados vão ser transferida entre a host e guest machine.

**db.vm.provision "shell", inline: <<-SHELL**: definir aprovisionamento especifico da shell da máquina virtual db
**wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar**: Wget é uma ferramenta da linha de comandos que permite o download de ficheiros e interacção com API REST. Suporta os seguintes protocolos da internet HTTP , HTTPS , FTP , and FTPS. Esta ferramenta pode funcionar com ligações à internet lentas e instáveis. No caso de falhar o wget continua a tentar até ter o ficheiro completo.

Para se poder depois aceder à base de dados será necessário usar: jdbc:h2:tcp://192.168.33.11:9092/./jpadb

**db.vm.provision "shell", :run => 'always', inline: <<-SHELL**: definir aprovisionamento. Por defeito o comando provision apenas é executado uma vez na configuração das máquinas, no entanto, neste caso usou-se o comando  **:run => 'always'** para correr sempre
####### java -cp ./h2*.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists > ~/out.txt &: 
define qual o caminho para as classes.

##### configuração da segunda máquina virtual (web)

    # Configurations specific to the webserver VM
    config.vm.define "web" do |web|
    web.vm.box = "envimation/ubuntu-xenial"
    web.vm.hostname = "web"
    web.vm.network "private_network", ip: "192.168.56.10"

Aqui as configurações são semelhantes à máquina anterior, com algumas diferenças, esta máquina vai chamar-se web e o IP vai ser diferente 192.168.56.10
        
    # We set more ram memmory for this VM
        web.vm.provider "virtualbox" do |v|
          v.memory = 1024
        end

Neste ponto definimos o provider para ser o hypervisor virtualbox e definimos a memória RAM para esta máquina virtual

    # We want to access tomcat from the host using port 8080
        web.vm.network "forwarded_port", guest: 8080, host: 8080

Aqui definimos o porto pelo qual vamos aceder ao tomcat, desde a máquina real.
    
        web.vm.provision "shell", inline: <<-SHELL, privileged: false
          sudo apt-get install git -y
          sudo apt-get install nodejs -y
          sudo apt-get install npm -y
          sudo ln -s /usr/bin/nodejs /usr/bin/node
          sudo apt install tomcat8 -y
          sudo apt install tomcat8-admin -y
          # If you want to access Tomcat admin web page do the following:
          # Edit /etc/tomcat8/tomcat-users.xml
          # uncomment tomcat-users and add manager-gui to tomcat user

Alterar o aprovisionamento desta máquina virtual
    
          # Change the following command to clone your own repository!

Alterado para o meu repositorio. Como andei a tentar acertar o Vagratfile e aplicação inicialmente coloquei o comando **rm -rf devops-21-22-lmn-1211753** mas não precisa de estar. Este comando removia a pasta do repositorio pois se já tivesse criada não dava para fazer clone outra vez.

          git clone https://Caroli1211753@bitbucket.org/caroli1211753/devops-21-22-lmn-1211753.git

Direcção para a pasta onde está a aplicação

          cd devops-21-22-lmn-1211753/CA3/part2/react-and-spring-data-rest-basic

Alterar permissões do ficheiro gradlew para se conseguir executar o comando

          chmod u+x gradlew

Execução do comando clean e build

          ./gradlew clean build

Aqui foi necessário actualizar o directório que tinha o ficheiro war e o nome do respectivo ficheiro. Este ficheiro é criado a primeira vez que se faz build.

          # To deploy the war file to tomcat8 do the following command:
          sudo cp ./build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps
        SHELL
    
    end

Além do Vagrantfile foi necessário alterar application.properties (src --> main --> resources). Neste ficheiro alterou-se a primeira linha para corresponder à aplicação

    server.servlet.context-path=/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT

Para se conseguir aceder às máquinas virtuais criadas usar, no browser da máquina host:

    http://localhost:8080/basic-0.0.1-SNAPSHOT/
    http://192.168.56.10:8080/basic-0.0.1-SNAPSHOT/
    
    http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console
    http://192.168.56.10:8080/basic-0.0.1-SNAPSHOT/h2-console
    For the connection string use: jdbc:h2:tcp://192.168.56.11:9092/./jpadb

In the host you can open the spring web application using one of the following options:

**vagrant ssh <VM name>**

Depois de alterar o vagrantfile profundamente é preciso fazer **git add**, **git commit** e **git push**. 
Para destruir as máquinas virtuais criadas **vagrant destroy** e outra vez **vagrant up** para criar com as novas configurações.

#### Alternativa VMWare

Instalação de VMWarePlayer https://customerconnect.vmware.com/en/downloads/info/slug/desktop_end_user_computing/vmware_workstation_player/16_0
Além do VMWare também é necessário instalar Plugin neste endereço https://www.vagrantup.com/vmware/downloads

Enquanto na virtualBox nós definimos a rede privada onde máquinas estavam ligadas o VNWare cria a sua própria rede, por isso, foi necessário verificar qual a rede em: Painel de Controlo --> Rede e Internet --> Ligações de rede verificar qual a rede criada pelo VNMWare. Neste caso: 192.168.145.1 , Mascara de rede 255.255.255.0

No VagrantFile foi necessário actualizar:

* a box para suportar VMWare e jdk11


    config.vm.box = "hashicorp/bionic64"
    config.vm.box_version = "1.0.282"

* o ip de cada máquina virtual criada para corresponder à rede criada pelo VMWare:
No caso da máquina db:


    db.vm.network "private_network", ip: "192.168.145.11"

No caso da web:


    web.vm.network "private_network", ip: "192.168.145.10"


Alterou-se o provider:

    web.vm.provider "vmware_desktop" do |v|
    v.memory = 1024
    end

Além do Vagrantfile foi necessário alterar application.properties (src --> main --> resources). Neste ficheiro alterou-se a primeira linha para corresponder à aplicação e também  *spring.datasource.url=* para corresponder ao ip da máquina virtual.

    server.servlet.context-path=/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT

    spring.datasource.url=jdbc:h2:tcp://192.168.145.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE


##### VirtualBox e VMWare

Em termos de funcionamento são semelhantes, ambos utilizei o Vagrantfile com algumas alterações.

No sector do IT, Oracle e VMWare são líderes em soluções de virtualização. VirtuaçBox é um hypervisor da Oracle para correr máquinas virtuais enquando VMWare em vários produtos para correr máquinas virtuais em vários cenários. 

###### VirtualBox
VirtualBox é o software de virtualização open-source mais popular que permite aos desenvolvedores correr vários sistemas operativos na mesma máquina. Este software é usado pelas equipas para que têm custos de operacionalização baixas. É a única solução profissional que é gratuita disponível como software open source. É útil para soluções de teste, desenvolvimento, demonstração e implementação em muitas plataformas e acessível a muitos sistemas operativos. Também conhecida pela rapidez e agilidade.

###### Alguns recursos de VirtualBox

* Open Source e software gratuito de virtualização: consegue funcionar em praticamente todos os sistemas operativos;

* Portabilidade: funciona em sistemas operativos 32-bit e 64-bit baseados em processadores Intel x86-64. É um hypervisor tipo 2. Funciona da mesma forma em todos os sistemas operativos. 

* Não é necessário hardware de virtualização: funciona em hardware mais antigo.

###### Vantagens

* Maior eficácia e custos mais baixos

* Instalação e configuração simples

* Amigo do utilizador

* Personalização

###### Desvantagens

* Depende da máquina hospedeira: consegue-se criar máquina virtual tão forte quanto a hospedeira

* Afectado pelas falhas da máquina hospedeira

* Menos eficiente: máquinas virtuais na virtualbox são ineficientes quando comparadas com máquinas reais.

###### VMWare

É um software hypervisor desenvolvido pel VMWare que é instalado nos servidores físicos e permite diferentes sistemas operativos na mesma máquina. No memo servidor físico, todas as máquinas virtuais partilham recursos como rede e RAM.

###### Alguns recurosos de VMWare

* Cria rede exclusiva

* Multi-Cloud

* Digital Worksplace

##### Vantagens

* Maior eficiencia 

* Utilização do espaço do centro de dados optima

* Fácil de voltar atrás no aprovionamento

* A adição de novas máquinas virtuais é simples

##### Desvantagens

* Requisitos de utilização: alguém com pouca experiência poderão ter dificuldades em instalar correctamente

* Não é a melhor opção para testar completamente teste físico

* baixa eficiencia

###### Principais diferenças

A diferença maior é que VirtualBox é um software open source enquanto que VMWare é apenas disponível para uso pessoal. A versão grtuita para uso pessoal ou educação tem funcionalidade limitada. Não suporta clones ou snapshots, por exemplo. VMWare também não disponibiliza virtualização de software enquanto VirtualBox permite.
A VirtualBox suporta uma variedade de tipos de disco (VMDK, VHD, HDD e QED) assim como ferramentas de integração como Vagrant e Docker enquanto que VMWare não suporta tantos tipos diferentes.
As máquinas virtuais criadas por VMWare são mais rápidas do que as de VirtualBox. Em projectos pequenos pode não ter influencia mas em projectos grandes isto pode influenciar a performance.



| Funcionalidades | VirtualBox | VMWare  |
|:-------------  |:--------------------- |:------------|
| Virtualização de hardware e Software | Permite virtualização de ambos (hardware e software)|Permite apenas virtualizaçõa de hardware|
| Sistema Operativo da máquina hospedeira | está disponível para Linux, Windows, Solaris, macOS, entre outros. | Está disponível para Linux e Windows. Para Mac é necessária uma versão Fusion ou Pro | 
| Sistemas Operativos da máquina virtual |	Podem ser instalados Linux, Windows, Solaris, FreeBSD e macOS |  Apenas suporta a instalação de Windows, Linux, Solaris e Mac |  
| Formato do disco virtual |	Suposta VDI (Virtual Disk Image), VMDK (Virtual Machine Disk) e VHD (Virtual Hard Disk) |  suporta VMDK |
| Interface User-friendly |	Interface de fácil utilização |  Interface complicada de utilizar |
| Suporte USB | É necessário um extension pack para ter a funcionalidade USB | suporta USB |
| Snapshots | Suporta snapshots, o que quer dizer que permite guardar e restaurar o estado de uma máquina virtual | apenas suportado na versão paga |
| Modos de rede | disponíveis: Not attached, Network Address Translation (NAT), NAT Network, Bridged networking, Internal networking, Host-only networking, Generic networking, UDP Tunnel, Virtual Distributed Ethernet (VDE) | disponíveis: Network Address Translation (NAT), Host-only networking, Virtual network editor (apenas em VMware workstation and Fusion Pro) |
| Open Source | é open source | não é open source |
                          |
