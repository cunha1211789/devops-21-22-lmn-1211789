CA3 part-1;

Creation and manual configuration of a virtual machine

1- Oracle Virtual box was downloaded for this particualr exercise

2- In the first part of the exercise the virtual machine was manually created.

3- A Linux ubuntu 64 bits was selected to be created.

4- Two gigabytes of RAM memory was allocated for the virtual machine, this means that the VM will only use two gigabytes maximum of the base computers available memory.

5- Also, ten gigabytes were designated as the maximum hard drive disk space allowed for the VM. These values are all virtual simulations of the physical hardware of the VM, meaning they use parts of the
actual base computer hardware with the established maximums for RAM and Harddrive. The VM files exist whithin the base computers HD.

6- An iso file(cd image file) was downaloaded in order to install the selected version of the OS.

7- At the VM settings the iso file was "loaded" into the virtual cd-drive, so that on startup the iso file was loaded and the instalation process could begin.

8- As the VM stars loading the iso file the initial installation process begins. A series of configuration choices were selected.

9- After the VM finised installing it was possible to access it. The installed ubuntu OS was command line only, with no grafical interface. 

10- Initialy the VM was a default network adapter that allows it to acess the internet. A second aditional network adapter was setup up so that the VM could connect with the physical host computer.

11- While in the linux command line sudo apt update command was used to update all the apilcations.

12- Also sudo apt install net-tools. With this package we can configure the addresses of the second network adapter. The command sudo nano /etc/netplan/01-netcfg.yaml was used in order to edit the configuration that
allows for the host computer to connect to the VM by setting the IP address(192.168.56.5). After edeting the configuration file sudo netplan apply was used so that the aplication would reboot.

13- In order to access the VM remotly the command sudo apt install openssh-server was used. After the configuration file was updated  with sudo nano /etc/ssh/sshd_config. The following line was uncommented: Password Authentication yes.

14- The service was restarted with sudo service ssh restart.

15- The vsftpd application was intalled with sudo apt install vsftpd so that files could be transfered to and from the VM. After the configurations were edited the aplication was also restarted.

16- With all this the VM is finally prepared and configured.

17- With ssh the VM was remotly accessed trought the oc command line by inputting the previously established IP and the login details(name and pass).

18- Finally git an java was installed within the VM. sudo apt install git and sudo apt install openjdk-8-jdk-headless commands were used for this porpuse.

19- With git installed the tut-react-and-spring-data-rest was cloned into the VM, and the application was booted in the VM itself, allowing it to run. Trough the host browser the appilcation can be accessed trough the IP 
and the virtual network that was setup before.


Final note:
Virtualization machines are incredebly usefull, as they allow you to use diferent OS whithin the same computer at the same time and virtualization of different hardware components. 
VMs work as virtual computers or software-defined computers within physical servers, existing only as code.
This makes it possible to run Linux for example, on a Windows OS, or to run an earlier version of Windows on more current Windows OS.
Also since VMs are independent of each other, this makes them portable. You can move a VM on a virtualBox to another virtualBox on a completely different machine almost instantaneously.
However manualy configuring several system each time, makes the process timeconsuming and tiresome. To this effect the next part of the exercise was foccused on tackilng this issue with the vagrant software.



Part 2 - Installing VM using vagrant files

1- Frist vagrant software was downloaded and installed. 

2- Then vagrant init envimation/ubuntu-xenial command was used to setup a vagrant project based on ubuntu 16.04. This creates the initial vagrant file setup for the specified OS. The specified OS is the envimation
/ubuntu-xenial that vagrant obtains from a library from the network.

3- Added a specific vagrant vm configuration box using vagrant box add envimation/ubuntu-xenial http://www.dei.isep.ipp.pt/~alex/publico/ubuntu-xenial-virtualbox.box. This sellects the speci
box to download from the url.

4- Used vagrant up to start the vm. The process of creating the VM is a lot faster with this tool, since the configurations of the VM is managed automatacly by vagrant.

5- Opened a secure shell of the vm using vagrant ssh.

6- With all this the VM is setup and ready to be accessed.

Final note:
This process is much more simple than all the previous steps for setting up the VM, since the vagrant file already has all the configurations detailed in the setup file. With this, by simply runnig the initial
vagrant file trough the command line, the configuration of the VM is done automatacly. All that is needed is the download of the specific vagrant file for the intended OS. Any aditional configuration can be added by editing
the vagrant file before running it.



part 3 - running a web aplication and database on two VM's

1- Downloaded the repository with the vagrant file already setup for creating two VM's, one for the web application and another for the database. The downloaded repository was available at github vagrant basic example.
This file aready has two sections of provision configurations specific for each VM.

2- The vagrant file has a provision configuration that forwards a port from the VM to the base computer and sets up a static private IP on the VM. It sets up two different IP adresses and maps them 
so that the host can access them. Provisioning is all tasks related to deployment and configurations of applications making them ready to use, wich speeds up deployment and eliminates human error.

3- The database was given the 192.168.56.10 address. The web application was set up on the 192.168.56.11.

4- One extra step was made so that the repository that the VM will download will be my specific repository (git clone https://Cunha1211789@bitbucket.org/cunha1211789/devops-21-22-lmn-1211789.git). This will allow the 
apilcation VM to run my personal aplication instead of the default one. This was done by altering the commands in the provision steps of the vagrant file, so that the git clone had the correct adress and the 
chande directory command would lead to the right file path.

5- The provision steps also configure the app to run in the tomcat server instead of the spring-boot developer mode.

6- Finally the both VM can be accesed trought the browser using the urls:

http://localhost:8080/basic-0.0.1-SNAPSHOT/
http://192.168.56.10:8080/basic-0.0.1-SNAPSHOT/

http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console
http://192.168.56.10:8080/basic-0.0.1-SNAPSHOT/h2-console

7- Finally, this vagrant file was pushed to my personal ropository.

Final note:
Since vagrant builds on top of already existing VM's and does the configuration and installation in an automatic way. This makes it easy  to define and share a different applications or environment setup in a 
single text file called Vagrantfile, eliminating the need to try and manually replicate every setup step when setting up a predifened web application.


Alternative Software:

Vagrant files work with other VM's other than virtualbox, which was the one used for the porpuses of this exercise. 
For this in the vagrant file the provider must be specified, for example the command: config.vm.provider "hiperv" will specify that the VM software to use will be hiperv. 
However, for this effect, the vagrant file must be compatible with the intended VM to use. Has long as the script of the vagrant file is compatible the setup of the VM will work exactly the same, 
making vagrant is an incredibly versatile tool.