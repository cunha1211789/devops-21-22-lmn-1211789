CA2 part 1 report

1- Cloned the gradle_basic_demo repository

2- With the help of the readme file inside the repository used the graddle wrapper (gradlew build) to build the project without the neccessaty of installing graddle

3- used the java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp command on the 59001 port to run the server

4- ran the clientRun to connect to the chat service as a Client

5- Added a new task called runClientServer that ChatServerApp instead of the ChatClientApp

6- Added a new dependency (testImplementation 'junit:junit:4.12')to graddle.build in order to run the test 

7- ran the test using gradlew test (no new task was created since graddle already had this default task)

8- created a new copy task that copies the srouce folder into the build folder

9- created a new myZip task that creates a zip file into the build folder

10- pushed the changes to the git devOps repository with all the changes and added the 'ca2-part1' tag


CA2 Part 2 report

1- Created a new branch named Part_2

2- Downloaded a new gradle spring boot project woth the following dependencies: Rest
Repositories; Thymeleaf; JPA; H2.

3- Extracted the zip contents of the spring boot project to a new folder(part_2) in the CA2 folder. 

4- Copied the src folders from the previous exercises replacing the scr folder from this sringboot zip.

5- Added the webpack.config.js and package.json files as well.

6- Used the gradlew bootRun. The page didnt load.

7- After adding the id "org.siouan.frontend-jdk11" version "6.0.0" plugin.

8- Added the following configuration:

 frontend {
nodeVersion = "14.17.3"
assembleScript = "run build"
cleanScript = "run clean"
checkScript = "run check"
}

and the following scripts:

frontend {
nodeVersion = "14.17.3"
assembleScript = "run build"
cleanScript = "run clean"
checkScript = "run check"
}

9- Built the project using gradlew build.

10- executed the gradlew bootRun command and the page loaded correctly.

11- Created a task to copy the generated jar file to a new folder named dist.

12- Created a task to delete the contents of the built folder. 

13- Added a condition that the clean task depended on the delete task ( clean.dependesOn delete)

14- Merged the new branch with the master branch.

15- Added, commited and pushed the changes to the remote.

