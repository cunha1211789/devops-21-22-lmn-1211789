CA5 part 1 - Jenkins

Jenkins is an open-source server that is written entirely in Java. It lets you execute a series of actions to achieve the continuous integration process in an automated fashion.
It facilitates continuous integration and continuous delivery in software projects by automating parts related to build, test, and deployment. Jenkins automates the software 
builds in a continuous manner and lets the developers know about the errors at an early stage. 
With jenkins its possible to create a pipeline wich contains one or more stages. 
A stage block defines distinct subset of tasks performed through the entire Pipeline (e.g. "Build", "Test" and "Deploy" stages), which is used by many plugins to visualize or 
present Jenkins Pipeline status.

For this exercise, a jenkins server was created and implemented.

1- First Jenkins was downloaded at https://www.jenkins.io/download/.

2- Then, using the java -jar jenkins.war command in cmd at the specified folder location, the jenkins server was initialized.

3- After the server is setup we need to create a jenkinsfile that will contain a set of instructions for the server to run. In these instructions we will set the fases
for the pipeline. Originaly there were 3 stages (chekout, build and Arquive) but for the porpuses of this exercise we subdivided the build fase into 2 stages, assembling and testing.
Assemble stage uses the 'gradlew clean assemble' cmd and the test stage uses the 'gradlew test' to run the tests.

4- Also the post instruction was set in the jenkins file so that the test results .xml file would be created.

5- After the jenkinsfile creation, in the browser, the jenkins server is accessible at localhost:8080. There, a user can be created and manage the server. 

6- After the user creation a new item can be created in the form of a new pipeline. The pipeline was established with its source from a git repository, and the path to the jenkinsfile was specified within the 
repository.

7- After the creation of the pipeline we can finally build the project and see if all the stages are being correctly built.

8- In this case the pipeline(CA5 part1) downloads the repository and int the folder of CA5 it executes the jenkinsfile that itself checks a specific folder in the repository (CA5). It runs 
the several stages in the file and if they are all building the gives the ok that the projects is building correctly.


part 2 docker image

1- For this part (part2) a docker image was added to the process so that in one of the stages jenkins would build and check a container, and afterwards would push it to dockerhub.

2- For this extra stages were necessary in the jenkinsfile. Namely, javadoc for the creation of all the documentation of the aplication, and docker image wich creates and pushs the image to docker hub.

3- Javadoc stage uses the 'gradlew javadoc' cmd to create the .html files with all te documentation.

4- For the docker image stage it was a bit more complex. The command used was 'docker.withRegistry' that specifies the hub credentials and the image to be built by jenkins.

5- After the file was set, in the kenkins server the docker hub credentials weere added so that jenkins can acess m account in dockerhub and push the image.

6- After a few tries, jenkins was able to pass and build all the stages and push the docker image to dockerhub.



Alternative Software -  GOcd

GOCD is a jenkins alternative software that assists teams and organizations in automating the continued delivery of software. It supports the automation of the whole build-test-release process.
Gocd has a few diferences from jenkins, for example, its able to perform jobs within the stages. These Jobs can run parralel to each other on a given stage.

1- First i installed GOCD on my computer. GOCD requires the instalation of two components, the server and the agent. 

2- After the instalation the software automatacly establish's a localserver at the port :8153. From here we can configure a new go pipeline.

3- The localhost server page enables the creation of a new pipeline. Unlike the previous exercise, at the creation of the new pipeline, we can configure the stages and tasks in a more intuitive
way, step by step by namming each stage, job and specifiyng each command. 

4- For this exercise i followed the GOcd toturial at https://www.lambdatest.com/blog/building-an-automated-testing-pipeline-with-gocd/.

5- The objective was to setup a sample Spring Boot application.

6- Tree stages were setup, the build project, Tomcat_setup and demo_deployment stage. 

7- This aproach was simple but for more complex setup it is possible to pass a yaml script the same as jenkins. we can do this by creating a new pipeline as Code.

8- In the end both softwares are similar in that they can achieve a good continuous automated build-test-realease process, with GOcd having a more intuitive and easy to use interface.

4- 